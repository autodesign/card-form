# Форма оплаты через банковские карты

## Установка

```sh
# Если еще не установлен yarn
npm install -g yarn
yarn install
```

*`npm install` тоже работает, но не рекомендуется.*

## Разработка

Для разработки используем `gulp dev`.

Сервер запускается на [http://localhost:13560](http://localhost:13560) (*порт указывается в `gulpfile.js`*).

Работает livereload для `css`
При изменении `html` перезагружается вся страница.

Править файлы:
* `src/styles.styl`
* `www/index.html`

Иконки складывать в `src/i/`. Шаблон для `svg2png` лежит в `src/icons/icons.styl.mustache`.

## Продакшн

`gulp build` соберет и минимизирует все внутри `www`. Папку целиком можно отправлять.
