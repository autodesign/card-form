var gulp         = require('gulp'),
    if_          = require('gulp-if'),
    replace      = require('gulp-replace'),
    plumber      = require('gulp-plumber'),
    stylus       = require('gulp-stylus'),
    sourcemaps   = require('gulp-sourcemaps'),
    postcss      = require('gulp-postcss'),
    imagemin     = require('gulp-imagemin'),
    baseimg      = require('gulp-baseimg'),
    svg2png      = require('gulp-svg2png'),
    csso         = require('gulp-csso'),
    newer        = require('gulp-newer'),
    size         = require('gulp-size'),
    debug        = require('gulp-debug'),

    path         = require('path'),

    es           = require('event-stream'),
    runSequence  = require('run-sequence'),

    autoprefixer = require('autoprefixer'),
    initial      = require('postcss-initial'),

    browserSync  = require('browser-sync').create();


var DEBUG = process.env.NODE_ENV !== 'production';


function styles(src, dest) {
  return function () {
    return gulp.src(src)
      .pipe(if_(DEBUG, newer({dest: dest})))
      .pipe(plumber())
      .pipe(if_(DEBUG, sourcemaps.init()))
      .pipe(stylus({
          'include css': true,
          url: {
              name: 'embedurl'
          },
      }))
      .pipe(postcss([initial({replace: true}), autoprefixer()]))
      .pipe(csso({comments: false}))
      .pipe(if_(DEBUG, sourcemaps.write()))
      .pipe(gulp.dest(dest))
      .pipe(if_(DEBUG, browserSync.stream()))
      .pipe(size({title: '(style) size:', showFiles: true}));
  }
}

function svgConvert(src, dest, template, icons) {
  return function() {
    var source = gulp.src(src)
      .pipe(debug({title: 'Optimizing SVG'}))
      .pipe(imagemin([
        imagemin.svgo({plugins: [{removeViewBox: true}]})
      ]))

    var baseImage = source
      .pipe(debug({title: 'Genering .styl from'}))
      .pipe(baseimg({
        styleTemplate: template,
        styleName: path.basename(icons),
      }))
      .pipe(replace("'url(", "url('")).pipe(replace(")'", "')"))
      .pipe(gulp.dest(path.dirname(icons)))
      .pipe(if_(DEBUG, browserSync.stream()));

    var png = source
        .pipe(debug({title: 'Converting to PNG'}))
        .pipe(newer({dest: dest, ext: '.png'}))
        .pipe(svg2png(/*scale=*/undefined, /*verbose=*/false, /*concurrency=*/4))
        .pipe(if_(!DEBUG, imagemin()))
        .pipe(gulp.dest(dest))
        .pipe(if_(DEBUG, browserSync.stream()))
        .pipe(size({title: '(png) size:'}))

    return es.merge(baseImage, png);
  }
}

var config = {
  port: 13560,
  css: {
    src: 'src/*.styl',
    dest: 'www/css/',
  },
  svg: {
    src: 'src/i/*.svg',
    dest: 'www/img/',
    template: 'src/icons/icons.styl.mustache',
    icons: 'src/icons/_icons.styl',
  },
};

gulp.task('svg', svgConvert(config.svg.src, config.svg.dest, config.svg.template, config.svg.icons));
gulp.task('styles', styles(config.css.src, config.css.dest));

gulp.task('dev', ['styles'], function() {
  browserSync.init({
    ui: false,
    open: false,
    port: config.port,
    host: '0.0.0.0',
    server: {
      baseDir: "./www/",
    },
  });

  gulp.watch([config.svg.src, config.svg.template], ['svg']);
  gulp.watch(config.css.src, ['styles']);
  gulp.watch("www/*.html").on('change', browserSync.reload);
  gulp.watch("www/js/*.js").on('change', browserSync.reload);
});

gulp.task('build', function(callback) {
  return runSequence(
    'svg',
    'styles',
    callback
  );
});
